'use strict';

/* jshint browser: true */

// Saves options to chrome.storage.sync.
function save_options() {
  var status = document.getElementById('status');

  status.innerHTML = '';
  status.className = '';

  try {
    var hosts = widgets.hosts.getValues();
    var project_values = widgets.projects.getValues();
    var projects = project_values.reduce(function (acc, value) {
      acc[value[0]] = value[1];
      return acc;
    }, {});
    var show_widget = document.getElementById('show_widget').value;
    var use_popup = document.getElementById('use_popup').value;
    var params = {
      hosts: hosts,
      projects: projects,
      show_widget: show_widget,
      use_popup: use_popup
    };

    chrome.storage.sync.set(params, function () {
      // Update status to let user know options were saved.
      status.textContent = 'Options saved.';
      status.className = 'success';
    });

  } catch (exception) {
    status.textContent = 'Invalid options.\n' + exception.message;
    status.className = 'error';
  }

  // Clear the status message after 5 seconds.
  setTimeout(function() {
    status.textContent = '';
  }, 5000);
}

// Restores preferences stored in chrome.storage.
function restore_options() {
  // Use default value hosts = 'gitlab.com,github.com'.
  chrome.storage.sync.get({
    'hosts': ['https://gitlab.com', 'https://github.com'],
    'projects': { 'example/website': 'EXAMPLE-01' },
    'show_widget': 'no',
    'use_popup': 'no'
  }, function (conf) {
    document.getElementById('show_widget').value = conf.show_widget;
    document.getElementById('use_popup').value = conf.use_popup;

    // Build hosts configuration widget.
    conf.hosts.forEach(function(host) {
      widgets.hosts.addRow([host]);
    });

    // Build mapping configuration widget.
    Object.entries(conf.projects).forEach(function(entry) {
      var key = entry[0];
      var value = entry[1];
      widgets.projects.addRow([key, value]);
    });
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);
