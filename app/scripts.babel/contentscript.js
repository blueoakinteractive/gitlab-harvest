'use strict';

/**
 * Sets and injects frame.html into the DOM.
 */
function injectHarvestFrame() {
  chrome.storage.sync.get(['hosts'], function (items) {
    let url = window.location.href.replace('/-/', '/');
    let urlRegex, matches, gitPath, issue, name, title, search, wrapper, frameURL, iframe, headerSearch, searchData;

    if (items && items.hosts) {
      urlRegex = new RegExp('^(' + items.hosts.join('|').replace(/\./g, '\\.') + ')((?:\/[a-zA-Z0-9\\._-]+){2,})\/(issues|merge_requests)\/(\\d+)', 'i');
      matches = url.match(urlRegex);
      if (matches) {
        [gitPath, issue] = [matches[2], matches[4]];

        if (window.location.host !== 'github.com') {
          title = document.querySelector('meta[property="og:title"]').getAttribute('content');
          name = document.querySelector('meta[property="og:url"]').getAttribute('content');
          name = name.split('/').pop();
          wrapper = document.getElementsByClassName('content')[0];
        } else {
          title = document.getElementsByClassName('js-issue-title')[0].innerHTML.trim();
          name = document.getElementsByClassName('gh-header-number')[0].innerHTML;
          wrapper = document.getElementById('discussion_bucket').parentElement;
        }

        if (wrapper) {
          frameURL = `frame.html?issue=${issue}&url=${encodeURIComponent(url)}&gitPath=${encodeURIComponent(gitPath)}&title=${encodeURIComponent(title)}&name=${encodeURIComponent(name)}`;

          iframe = document.createElement('iframe');
          iframe.src = chrome.runtime.getURL(frameURL);
          iframe.style.cssText = 'border:0; width:100%; height:450px;';
          wrapper.appendChild(iframe);
        }
      }
    }
  });
}

injectHarvestFrame();
