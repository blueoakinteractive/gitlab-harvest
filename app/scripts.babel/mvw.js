'use strict';
/* jshint browser: true */

// Simple widget to handle a simple mutli value table.
class Mvw {

  /**
   * Add row to the widget.
   * @param  {string} id The id of the widget.
   * @param  {int} columnsLength The default columns lenght of the widget.
   */
  constructor(id, columnsLength) {
      this.id = id;
      this.columnsLength = columnsLength;
  }

  /**
   * Add empty row to the widget.
   */
  addEmptyRow() {
    let columns = [];
    for (let i = 0; i < this.columnsLength; i++) {
      columns.push('');
    }
    this.addRow(columns);
  }

  /**
   * Add row to the widget.
   * @param  {array} columns The columns.
   */
  addRow(columns) {
    let container = document.getElementById(this.id);
    let row = document.createElement('tr');

    for (let column in columns) {
      let td = document.createElement('td');
      let input = document.createElement('input');
      input.value = columns[column];
      td.appendChild(input);
      row.appendChild(td);
    }
    let std = document.createElement('td');
    std.setAttribute('width', 1);
    let button = document.createElement('button');
    let t = document.createTextNode('Remove');
    button.appendChild(t);
    button.className = 'grey';
    std.appendChild(button);
    row.appendChild(std);
    button.addEventListener('click', mvw_remove_row_handler);
    container.appendChild(row);
  }

  /**
   * Returns the values.
   * @return {array} Multi dimension array of all values.
   */
  getValues() {
    let container = document.getElementById(this.id);
    let rows = container.querySelectorAll('tr');
    let values = [];
    [].forEach.call(rows, function(row) {
      let inputs = row.querySelectorAll('input');
      let inner_values = [];
      [].forEach.call(inputs, function(input) {
        let value = input.value;
        if (value != '') {
          inner_values.push(value);
        }
      });
      if (inner_values.length != 0) {
        values.push(inner_values);
      }
    });
    return values;
  }
}

/**
 * Remove row event handler.
 * @param  {Event} event The event.
 */
function mvw_remove_row_handler (event){
  let button = event.target;
  button.parentNode.parentNode.remove();
}

/**
 * Add row handler event handler.
 * @param  {Event} event The event.
 */
function mvw_add_row_handler (event){
  let widget_id = event.target.getAttribute('data-mvw-add-row');
  widgets[widget_id].addEmptyRow();
}
let add_buttons = document.querySelectorAll('button[data-mvw-add-row]');
[].forEach.call(add_buttons, function(button) {
  button.addEventListener('click', mvw_add_row_handler);
});

let widgets = [];
widgets['hosts'] = new Mvw('hosts', 1);
widgets['projects'] = new Mvw('projects', 2);
